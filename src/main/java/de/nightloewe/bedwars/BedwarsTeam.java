package de.nightloewe.bedwars;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.entity.Player;

import de.maltesermailo.api.minigame.PlayerHandle;
import de.maltesermailo.impl.minigame.MapLocation;

public class BedwarsTeam {

	private String name;
	private String displayName;
	
	private int maxPlayers;
	
	private MapLocation teamSpawn;
	private MapLocation bedLocation;
	
	public BedwarsTeam(String name, String displayName, int maxplayers, Location teamSpawn, Location bedLocation) {
		this.name = name;
		this.displayName = displayName;
		this.teamSpawn = new MapLocation(teamSpawn);
		this.maxPlayers = maxplayers;
		this.bedLocation = new MapLocation(bedLocation);
	}
	
	public String getDisplayName() {
		return displayName;
	}
	
	public String getName() {
		return name;
	}
	
	public Location getTeamSpawn() {
		return teamSpawn.get();
	}
	
	public Location getBedSpawn() {
		return bedLocation.get();
	}
	
	public int getMaxPlayers() {
		return maxPlayers;
	}
	
	public void setDisplayName(String displayName) {
		this.displayName = displayName;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public void setTeamSpawn(Location teamSpawn) {
		this.teamSpawn = new MapLocation(teamSpawn);
	}
	
	public void setBedSpawn(Location bedLocation) {
		this.bedLocation = new MapLocation(bedLocation);
	}
	
	public void setMaxPlayers(int maxPlayers) {
		this.maxPlayers = maxPlayers;
	}
	
	public int getTeamSize() {
		int size = 0;
		for(Player p : Bukkit.getOnlinePlayers()) {
			PlayerHandle handle = Bedwars.instance().context().miniGameController().getPlayerHandle(p);
			
			BedwarsPlayerData data = handle.getPlayerData();
			
			if(handle.isSpectator())
				continue;
			
			if(data.getTeam() == null)
				continue;
			
			if(data.getTeam().getName().equalsIgnoreCase(this.name))
				size++;
		}
		return size;
	}
	
	public boolean hasBed() {
		return this.getBedSpawn().getBlock().getType() == Material.BED_BLOCK;
	}
	
	public List<PlayerHandle> getPlayers() {
		List<PlayerHandle> players = new ArrayList<PlayerHandle>();
		for(Player p : Bukkit.getOnlinePlayers()) {
			PlayerHandle handle = Bedwars.instance().context().miniGameController().getPlayerHandle(p);
			if(!handle.isSpectator()) {
				BedwarsPlayerData data = handle.getPlayerData();
				
				if(data.getTeam() != null && data.getTeam() == this) {
					players.add(handle);
				}
			}
		}
		return players;
	}
	
}
