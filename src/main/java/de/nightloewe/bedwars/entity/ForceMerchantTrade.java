package de.nightloewe.bedwars.entity;

import org.bukkit.entity.Player;
import org.bukkit.inventory.InventoryView;
import org.bukkit.inventory.ItemStack;

import de.nightloewe.bedwars.util.CloudPackage;

import java.lang.reflect.InvocationTargetException;

/**
 * Reflection based Version of ForceVillagerTrade.
 * Created custom Trade Inventories.
 */
public class ForceMerchantTrade {

    private static Class<?> entityHuman;
    private static Class<?> merchantRList;
    private static Class<?> iMerchant;
    private static Class<?> merchantRecipe;
    private static Class<?> craftItemStack;
    private static Class<?> nmsItemStack;
    private static Class<?> craftPlayer;
    private static Class<?> chatSerializer;
    private static Class<?> craftInventoryView;
    private static Class<?> containerMerchant;
    private static Class<?> inventoryMerchant;
    private static Class<?> packetDataSerialzier;
    private static Class<?> customPayload;

    static {
        try {

            entityHuman = CloudPackage.MINECRAFT.getDynClass("EntityHuman");
            merchantRList = CloudPackage.MINECRAFT.getDynClass("MerchantRecipeList");
            iMerchant = CloudPackage.MINECRAFT.getDynClass("IMerchant");
            merchantRecipe = CloudPackage.MINECRAFT.getDynClass("MerchantRecipe");
            craftItemStack = CloudPackage.CRAFTBUKKIT.getDynClass("inventory.CraftItemStack");
            nmsItemStack = CloudPackage.MINECRAFT.getDynClass("ItemStack");
            craftPlayer = CloudPackage.CRAFTBUKKIT.getDynClass("entity.CraftPlayer");
            chatSerializer = CloudPackage.MINECRAFT.getDynClass("IChatBaseComponent$ChatSerializer");
            craftInventoryView = CloudPackage.CRAFTBUKKIT.getDynClass("inventory.CraftInventoryView");
            containerMerchant = CloudPackage.MINECRAFT.getDynClass("ContainerMerchant");
            inventoryMerchant = CloudPackage.MINECRAFT.getDynClass("InventoryMerchant");
            packetDataSerialzier = CloudPackage.MINECRAFT.getDynClass("PacketDataSerializer");
            customPayload = CloudPackage.MINECRAFT.getDynClass("PacketPlayOutCustomPayload");
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
    }

    private String invname;
    private Object merchantRecipeList;
    private int size;

    /**
     * @param invname Inventory display name, (May contain color)
     */
    public ForceMerchantTrade(String invname) {
        this.invname = invname;
        size = 0;
        try {
            this.merchantRecipeList = merchantRList.getConstructor().newInstance();
        } catch (InstantiationException | IllegalAccessException |
                InvocationTargetException | NoSuchMethodException e) {
            e.printStackTrace();
        }
    }

    /**
     * @param in  The itemstack input
     * @param out The itemstack output.
     * @return ForceVillagerTrade object so you can invoke the next method like:
     * addTrade(...).addTrade(...).addTrade(...).openTrade(player);
     */
    public ForceMerchantTrade addTrade(ItemStack in, ItemStack out) {
        try {
            Object recipe = merchantRecipe.getConstructor(nmsItemStack, nmsItemStack, nmsItemStack, int.class, int.class)
                    .newInstance(getNMSItemStack(in), null, getNMSItemStack(out), 0, 2500);
            addMerchantRecipe(recipe);
            size++;
        } catch (InstantiationException | IllegalAccessException
                | InvocationTargetException | NoSuchMethodException e) {
            e.printStackTrace();
        }
        return this;
    }

    /**
     * @param inOne The itemstack in the first input slot.
     * @param inTwo The itemstack on the second input slot.
     * @param out   The itemstack output.
     * @return ForceVillagerTrade object so you can invoke the next method like:
     * addTrade(...).addTrade(...).addTrade(...).openTrade(player);
     */
    public ForceMerchantTrade addTrade(ItemStack inOne, ItemStack inTwo,
                                       ItemStack out) {
        try {
            Object recipe = merchantRecipe.getConstructor(nmsItemStack, nmsItemStack, nmsItemStack, int.class, int.class)
                    .newInstance(getNMSItemStack(inOne), getNMSItemStack(inTwo), getNMSItemStack(out), 0, 2500);
            addMerchantRecipe(recipe);
            size++;
        } catch (InstantiationException | IllegalAccessException
                | InvocationTargetException | NoSuchMethodException e) {
            e.printStackTrace();
        }
        return this;
    }

    /**
     * @param who The player who will see the Trade
     */
    public void openTrade(Player who, int position) {
        try {
            Object human = craftPlayer.getMethod("getHandle").invoke(who);
            Object chatComponent = chatSerializer.getMethod("a", String.class).invoke(null, "{\"text\": \"" + invname + "\"}");
            entityHuman.getMethod("openTrade", iMerchant).invoke(human,
                    new ProxyMerchant_1_9(human, merchantRecipeList, chatComponent).getProxyInterface());
            InventoryView view = who.getOpenInventory();
            Object container_m = craftInventoryView.getMethod("getHandle").invoke(view);
            containerMerchant.getMethod("d", int.class).invoke(container_m, position);
            containerMerchant.getMethod("b").invoke(container_m);
        } catch (IllegalAccessException | InvocationTargetException | NoSuchMethodException e) {
            e.printStackTrace();
        }
    }

    /**
     * Converts the ItemStack via Reflection into an NMS ItemStack.
     *
     * @param stack - The Stack to convert.
     * @return The Converted NMS ItemStack.
     */
    private Object getNMSItemStack(ItemStack stack) {
        try {
            return craftItemStack.getMethod("asNMSCopy", ItemStack.class).invoke(null, stack);
        } catch (IllegalAccessException | InvocationTargetException | NoSuchMethodException e) {
            e.printStackTrace();
            return null;
        }
    }

    /**
     * Adds the given Recipe the to MerchantRecipeList.
     *
     * @param recipe The MerchantRecipe to add.
     */
    private void addMerchantRecipe(Object recipe) {
        try {
            merchantRList.getSuperclass().getMethod("add", Object.class).invoke(merchantRecipeList, recipe);
        } catch (IllegalAccessException | InvocationTargetException | NoSuchMethodException e) {
            e.printStackTrace();
        }
    }


    //returns the Size of Recipes in the List.
    public int getSize() {
        return size;
    }
}
