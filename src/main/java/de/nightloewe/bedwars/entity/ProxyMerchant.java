package de.nightloewe.bedwars.entity;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;

import de.nightloewe.bedwars.util.CloudPackage;

/**
 * Reflection based class to use the IMerchant Class.
 */
public class ProxyMerchant {

    private static Class<?> iMerchant;

    static {
        try {
            iMerchant = CloudPackage.MINECRAFT.getDynClass("IMerchant");
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
    }

    private Object human;
    private Object merchantList;
    private Object chatComponent;

    /**
     * Creates an Reflection compatible Version of an IMerchant.
     *
     * @param human         - The EntityHumand to implement in IMerchant.
     * @param merchantList  - The MerchantRecipeList to implement in IMerchant.
     * @param chatComponent - The ChatComponent containing the Title to implement in IMerchant.
     */
    public ProxyMerchant(Object human, Object merchantList, Object chatComponent) {
        this.human = human;
        this.merchantList = merchantList;
        this.chatComponent = chatComponent;
    }

    /**
     * Returns the generated IMerchant Reflection.
     *
     * @return The generated IMerchant Reflection.
     */
    public Object getProxyInterface() {
        return Proxy.newProxyInstance(iMerchant.getClassLoader(), new Class[]{iMerchant}, new InvocationHandler() {
            @Override
            public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
                String method_name = method.getName();
                Class<?>[] classes = method.getParameterTypes();
                if (method_name.equals("getTrader")) {
                    //Return EntityHuman
                    return human;
                } else if (method_name.equals("getOffers")) {
                    //Merchant List
                    return merchantList;
                } else if (method_name.equals("getScoreboardDisplayName")) {
                    //Return the InventoryName
                    return chatComponent;
                }
                return null;
            }
        });
    }


}
