package de.nightloewe.bedwars.gamephases;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

import de.maltesermailo.api.Context;
import de.maltesermailo.api.minigame.AbstractMinigame;
import de.maltesermailo.api.minigame.GamePhase;
import de.maltesermailo.impl.minigame.gamephases.LobbyGamePhase;
import de.nightloewe.bedwars.gamephases.listener.AsyncPlayerChatListener;
import de.nightloewe.bedwars.gamephases.listener.CraftingListener;
import de.nightloewe.bedwars.gamephases.listener.JoinListener;
import de.nightloewe.bedwars.gamephases.listener.LegendaryNickListener;
import de.nightloewe.bedwars.gamephases.listener.LegendarySelfColorListener;
import de.nightloewe.bedwars.gamephases.listener.PlayerInteractListener;

public class BedwarsLobbyGamePhase extends LobbyGamePhase {

	private Context ctx;
	
	public BedwarsLobbyGamePhase(AbstractMinigame minigame) {
		super(minigame);
		
		this.ctx = minigame.context();
	}
	
	@Override
	public GamePhase next() {
		return this.ctx.miniGameController().getGamePhaseManager().getGamePhase("Ingame");
	}
	
	@Override
	public void init() {
		super.init();
		this.ctx.eventController().addListener(new JoinListener(this.ctx));
		this.ctx.eventController().addListener(new LegendaryNickListener());
		this.ctx.eventController().addListener(new LegendarySelfColorListener());
		this.ctx.eventController().addListener(new AsyncPlayerChatListener(this.ctx));
		this.ctx.eventController().addListener(new PlayerInteractListener(this.ctx));
		this.ctx.eventController().addListener(new CraftingListener());
	}

}
