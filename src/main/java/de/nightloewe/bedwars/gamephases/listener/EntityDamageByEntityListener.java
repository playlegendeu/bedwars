package de.nightloewe.bedwars.gamephases.listener;

import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.entity.Projectile;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageByEntityEvent;

import de.maltesermailo.api.Context;
import de.maltesermailo.api.minigame.PlayerHandle;
import de.nightloewe.bedwars.BedwarsPlayerData;

public class EntityDamageByEntityListener implements Listener {

	private Context ctx;

	public EntityDamageByEntityListener(Context ctx) {
		this.ctx = ctx;
	}
	
	@EventHandler
	public void onDamageByEntity(EntityDamageByEntityEvent e) {
		if(e.getEntityType() == EntityType.PLAYER
				&& e.getDamager().getType() == EntityType.PLAYER) {
			Player p = (Player) e.getEntity();
			Player damager = (Player) e.getDamager();
			
			PlayerHandle handle = this.ctx.miniGameController().getPlayerHandle(p);
			BedwarsPlayerData data = handle.getPlayerData();
			
			PlayerHandle dHandle = this.ctx.miniGameController().getPlayerHandle(damager);
			BedwarsPlayerData dData = dHandle.getPlayerData();

			if(data.getTeam() == dData.getTeam()) {
				e.setCancelled(true);
				return;
			}
			
			data.setLastPlayerHit(damager);
			data.setLastPlayerHitTimestamp(System.currentTimeMillis());
		} else if(e.getDamager() instanceof Projectile) {
			Projectile entity = (Projectile) e.getDamager();
			
			if(entity.getShooter() instanceof Player) {
				Player p = (Player) e.getEntity();
				Player damager = (Player) entity.getShooter();
				
				PlayerHandle handle = this.ctx.miniGameController().getPlayerHandle(p);
				BedwarsPlayerData data = handle.getPlayerData();
				
				PlayerHandle dHandle = this.ctx.miniGameController().getPlayerHandle(damager);
				BedwarsPlayerData dData = dHandle.getPlayerData();
				
				if(data.getTeam() == dData.getTeam()) {
					e.setCancelled(true);
					return;
				}
				
				data.setLastPlayerHit(damager);
				data.setLastPlayerHitTimestamp(System.currentTimeMillis());
			}
		}
	}
	
}
