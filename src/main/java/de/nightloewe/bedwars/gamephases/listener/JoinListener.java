package de.nightloewe.bedwars.gamephases.listener;

import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;

import de.maltesermailo.api.Context;
import de.maltesermailo.api.minigame.ManagedScoreboard;
import de.maltesermailo.api.minigame.PlayerHandle;
import de.maltesermailo.api.minigame.PlayerScoreboard;
import de.maltesermailo.impl.minigame.listener.items.GameSetupItem;
import de.nightloewe.bedwars.BedwarsMap;
import de.nightloewe.bedwars.BedwarsPlayerData;
import de.nightloewe.bedwars.BedwarsTeam;
import de.nightloewe.bedwars.gamephases.BedwarsLobbyGamePhase;
import de.nightloewe.bedwars.inventory.TeamChooserItem;
import net.md_5.bungee.api.ChatColor;

public class JoinListener implements Listener {

	private Context ctx;
	private GameSetupItem gameSetupItem;
	private TeamChooserItem teamChooserItem;
	
	public JoinListener(Context ctx) {
		this.ctx = ctx;
		this.gameSetupItem = new GameSetupItem(this.ctx);
		this.teamChooserItem = new TeamChooserItem(this.ctx);
	}
	
	@EventHandler
	public void onJoin(PlayerJoinEvent e) {
		PlayerHandle handle = this.ctx.miniGameController().getPlayerHandle(e.getPlayer());
		
		BedwarsPlayerData data = new BedwarsPlayerData();
		handle.setPlayerData(data);
		
		if(this.ctx.miniGameController().getGamePhaseManager().current() instanceof BedwarsLobbyGamePhase) {
			PlayerScoreboard sb = new PlayerScoreboard(e.getPlayer().getName(), "Bedwars");
			sb.setScore(1, "&1 ", 4);
			sb.setScore(2, "&7Map: ", 3);
			sb.setScore(3, "&c" 
					+ this.ctx.mapFactory().current().getMapName() 
					+ " &7(&e" + ((BedwarsMap) this.ctx.mapFactory().current()).getTeams().size() 
					+ "&7x&e" 
					+ ((BedwarsMap) this.ctx.mapFactory().current()).getTeams().get(0).getMaxPlayers() 
					+ "&7) ", 2);
			sb.setScore(4, "&2 ", 1);
			sb.setScore(5, "&7Team: ", 0);
			sb.setScore(6, "&cKein Team", -1);
			sb.setScore(7, "&3 ", -2);
			
			handle.setCurrentScoreboard(sb);
			
			if(e.getPlayer().hasPermission("minigame.forcemap")) {
				e.getPlayer().getInventory().setItem(4, this.gameSetupItem.getItem());
			}
			
			e.getPlayer().getInventory().setItem(0, this.teamChooserItem.getItem());
		} else {
			PlayerScoreboard sb = new PlayerScoreboard(e.getPlayer().getName(), "Bedwars");
			for(int i = 0; i < ((BedwarsMap) this.ctx.mapFactory().current()).getTeams().size(); i++) {
				BedwarsTeam t = ((BedwarsMap) this.ctx.mapFactory().current()).getTeams().get(i);
				sb.setScore(i, (t.hasBed() ? "&2✔" : "&4✕") + " " + t.getDisplayName(), t.getTeamSize());
			}
			handle.setCurrentScoreboard(sb);
			if(handle.isSpectator()) {
				e.getPlayer().teleport(this.ctx.mapFactory().current().getSpectatorSpawn());
			}
		}
	}
}
