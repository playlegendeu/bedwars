package de.nightloewe.bedwars.gamephases.listener;

import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;

import de.maltesermailo.api.minigame.PlayerHandle;
import de.maltesermailo.impl.nick.LegendaryNickEvent;
import de.nightloewe.bedwars.Bedwars;
import de.nightloewe.bedwars.BedwarsPlayerData;
import de.nightloewe.bedwars.BedwarsTeam;
import net.md_5.bungee.api.ChatColor;

public class LegendaryNickListener implements Listener {

	@EventHandler
	public void onNick(LegendaryNickEvent e) {
		if(!Bedwars.instance().context().miniGameController().getGamePhaseManager().isRunning()) {
			return;
		}
		
		Player p = e.getNicking();
		PlayerHandle handle = Bedwars.instance().context().miniGameController().getPlayerHandle(p);
		BedwarsPlayerData data = handle.getPlayerData();
		
		if(data.getTeam() != null) {
			BedwarsTeam team = data.getTeam();
			e.setPrefix(ChatColor.translateAlternateColorCodes('&', team.getDisplayName().substring(0,2)));
			e.setSuffix("");
			e.setRank(team.getDisplayName().charAt(1));
		}
		
		if(handle.isSpectator()) {
			e.setPrefix(ChatColor.translateAlternateColorCodes('&', "&7"));
			e.setSuffix("");
			e.setRank(99);
		}
	}
	
}
