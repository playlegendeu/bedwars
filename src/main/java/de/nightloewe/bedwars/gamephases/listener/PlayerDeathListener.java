package de.nightloewe.bedwars.gamephases.listener;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.PlayerDeathEvent;

import de.maltesermailo.api.Context;
import de.maltesermailo.api.minigame.PlayerHandle;
import de.maltesermailo.api.minigame.PlayerScoreboard;
import de.nightloewe.bedwars.Bedwars;
import de.nightloewe.bedwars.BedwarsMap;
import de.nightloewe.bedwars.BedwarsPlayerData;
import de.nightloewe.bedwars.BedwarsTeam;

public class PlayerDeathListener implements Listener {

	private Context ctx;
	
	public PlayerDeathListener(Context ctx) {
		this.ctx = ctx;
	}
	
	@EventHandler
	public void onPlayerDeath(PlayerDeathEvent e) {
		e.getDrops().clear();
		
		Player p = (Player) e.getEntity();
		Player killer = e.getEntity().getKiller();
		
		PlayerHandle handle = this.ctx.miniGameController().getPlayerHandle(p);
		BedwarsPlayerData data = handle.getPlayerData();
		BedwarsTeam team = data.getTeam();
		
		if(data.getLastPlayerHit() != null && data.getLastPlayerHitTimestamp() - System.currentTimeMillis() < 10000) {
			killer = data.getLastPlayerHit();
			data.setLastPlayerHit(null);
			data.setLastPlayerHitTimestamp(0);
		}
		
		if(killer != null) {
			Bedwars.instance().broadcastMessage("Der Spieler " + p.getDisplayName() + " &7wurde von " + killer.getDisplayName() + " &7getötet.");
			e.setDeathMessage("");
		} else {
			Bedwars.instance().broadcastMessage("Der Spieler " + p.getDisplayName() + " &7ist gestorben.");
			e.setDeathMessage("");
		}
		
		PlayerHandle kHandle;
		BedwarsPlayerData kData = null;
		if(killer != null) {
			kHandle = this.ctx.miniGameController().getPlayerHandle(killer);
			kData = kHandle.getPlayerData();
		}
		
		if(!team.hasBed()) {
			handle.setSpectator(true);
			data.setTeam(null);
			
			if(kData != null) {
				kData.addCoins(20);
			}
			
			for(Player all : Bukkit.getOnlinePlayers()) {
				PlayerHandle allHandle = this.ctx.miniGameController().getPlayerHandle(all);
				PlayerScoreboard sb = allHandle.getCurrentScoreboard();
				for(int i = 0; i < ((BedwarsMap) this.ctx.mapFactory().current()).getTeams().size(); i++) {
					BedwarsTeam t = ((BedwarsMap) this.ctx.mapFactory().current()).getTeams().get(i);
					if(t == team) {
						sb.removeScore(i);
						sb.setScore(i, "&4✕ " + t.getDisplayName(), t.getTeamSize());
					}
				}
			}
			
			p.teleport(this.ctx.mapFactory().current().getSpectatorSpawn());
			Bedwars.instance().sendMessage(p, "&cDu bist aus dem Spiel ausgeschieden");
		} else {
			p.spigot().respawn();
			Bedwars.instance().sendMessage(p, "Du konntest respawnen, da dein Bett noch existiert!");
		}
		
		if(handle.isSpectator()) {
			data.setTeam(null);
		}
		
		if(team.getTeamSize() == 0) {
			Bedwars.instance().broadcastMessage("&4Das Team " + team.getDisplayName() + " &4ist ausgeschieden");
		}
		
		BedwarsTeam potentiallyWinner = null;
		
		if(kData != null) {
			potentiallyWinner = kData.getTeam();
		}
		
		if(potentiallyWinner == null) {
			for(BedwarsTeam t : ((BedwarsMap) this.ctx.mapFactory().current()).getTeams()) {
				if(t.getTeamSize() > 0) {
					potentiallyWinner = t;
				}
			}
		}
		
		for(BedwarsTeam t : ((BedwarsMap) this.ctx.mapFactory().current()).getTeams()) {
			if(t.getTeamSize() > 0 && (potentiallyWinner != null && t != potentiallyWinner)) {
				return;
			}
		}
		
		for(Player all : Bukkit.getOnlinePlayers()) {
			PlayerHandle allHandle = this.ctx.miniGameController().getPlayerHandle(all);
			BedwarsPlayerData allData = allHandle.getPlayerData();
			
			allHandle.setCoins(allHandle.getCoins() + allData.getCoins());
			Bedwars.instance().sendMessage(all, "Du hast &a" + allData.getCoins() + " &7Coins erhalten.");
		}
		
		Bedwars.instance().broadcastMessage("&aDas Team " + potentiallyWinner.getDisplayName() + " &ahat gewonnen.");
		this.ctx.miniGameController().endGame();
	}
}
