package de.nightloewe.bedwars.gamephases.listener;

import org.bukkit.Material;
import org.bukkit.entity.EntityType;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerPickupItemEvent;

import de.maltesermailo.api.utils.ItemFactory;
import de.nightloewe.bedwars.entity.BedwarsResource;

public class PlayerPickupItemListener implements Listener {

	@EventHandler
	public void onPickupItem(PlayerPickupItemEvent e) {
		if(e.getItem().getType() == EntityType.ARROW) {
			e.setCancelled(true);
		}
		if(e.getItem().getItemStack().getType() == Material.CLAY_BRICK) {
			int amount = e.getItem().getItemStack().getAmount();
			e.getItem().setItemStack(ItemFactory.newFactory(BedwarsResource.BRONZE.getMaterial(), BedwarsResource.BRONZE.getName()).build(amount));
		} else if(e.getItem().getItemStack().getType() == Material.IRON_INGOT) {
			int amount = e.getItem().getItemStack().getAmount();
			e.getItem().setItemStack(ItemFactory.newFactory(BedwarsResource.SILVER.getMaterial(), BedwarsResource.SILVER.getName()).build(amount));
		} else if(e.getItem().getItemStack().getType() == Material.GOLD_INGOT) {
			int amount = e.getItem().getItemStack().getAmount();
			e.getItem().setItemStack(ItemFactory.newFactory(BedwarsResource.GOLD.getMaterial(), BedwarsResource.GOLD.getName()).build(amount));
		}
	}
	
}
