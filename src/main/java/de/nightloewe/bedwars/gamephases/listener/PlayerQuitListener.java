package de.nightloewe.bedwars.gamephases.listener;

import java.util.ArrayList;
import java.util.Arrays;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.inventory.ItemStack;

import de.maltesermailo.api.Context;
import de.maltesermailo.api.minigame.PlayerHandle;
import de.maltesermailo.api.minigame.PlayerScoreboard;
import de.nightloewe.bedwars.BedwarsMap;
import de.nightloewe.bedwars.BedwarsPlayerData;
import de.nightloewe.bedwars.BedwarsTeam;

public class PlayerQuitListener implements Listener {

	private Context ctx;

	public PlayerQuitListener(Context ctx) {
		this.ctx = ctx;
	}
	
	@EventHandler(priority = EventPriority.LOW)
	public void onQuit(PlayerQuitEvent e) {
		Player p = e.getPlayer();
		PlayerHandle handle = this.ctx.miniGameController().getPlayerHandle(p);
		
		if(!handle.isSpectator()) {
			BedwarsPlayerData data = handle.getPlayerData();
			BedwarsTeam team = data.getTeam();
			
			data.setTeam(null);
			
			for(Player all : Bukkit.getOnlinePlayers()) {
				PlayerHandle allHandle = this.ctx.miniGameController().getPlayerHandle(all);
				PlayerScoreboard sb = allHandle.getCurrentScoreboard();
				for(int i = 0; i < ((BedwarsMap) this.ctx.mapFactory().current()).getTeams().size(); i++) {
					BedwarsTeam t = ((BedwarsMap) this.ctx.mapFactory().current()).getTeams().get(i);
					if(t == team) {
						if(t.hasBed()) {
							sb.removeScore(i);
							sb.setScore(i, ChatColor.translateAlternateColorCodes('&', "&2✔ " + t.getDisplayName()), t.getTeamSize());
						} else {
							sb.removeScore(i);
							sb.setScore(i, ChatColor.translateAlternateColorCodes('&', "&4✕ " + t.getDisplayName()), t.getTeamSize());
						}
					}
				}
			}
			
			data.setTeam(team);
			handle.setSpectator(true);
			
			Bukkit.getPluginManager().callEvent(new PlayerDeathEvent(p, new ArrayList<ItemStack>(), 0, ""));
		} else {
			BedwarsPlayerData data = handle.getPlayerData();
			if(data.getTeam() != null) {
				Bukkit.getPluginManager().callEvent(new PlayerDeathEvent(p, new ArrayList<ItemStack>(), 0, ""));
			}
		}
	}
}
