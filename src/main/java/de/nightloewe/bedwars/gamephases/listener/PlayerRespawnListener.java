package de.nightloewe.bedwars.gamephases.listener;

import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerRespawnEvent;

import de.maltesermailo.api.Context;
import de.maltesermailo.api.minigame.PlayerHandle;
import de.nightloewe.bedwars.BedwarsPlayerData;

public class PlayerRespawnListener implements Listener {

	private Context ctx;

	public PlayerRespawnListener(Context ctx) {
		this.ctx = ctx;
	}
	
	@EventHandler
	public void onRespawn(PlayerRespawnEvent e) {
		Player p = e.getPlayer();
		PlayerHandle handle = this.ctx.miniGameController().getPlayerHandle(p);
		BedwarsPlayerData data = handle.getPlayerData();
		
		if(data.getTeam() != null) {
			e.setRespawnLocation(data.getTeam().getTeamSpawn());
		}
	}
}
