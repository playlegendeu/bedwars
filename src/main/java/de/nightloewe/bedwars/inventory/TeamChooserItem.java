package de.nightloewe.bedwars.inventory;

import org.bukkit.Bukkit;
import org.bukkit.DyeColor;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.Event.Result;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryDragEvent;
import org.bukkit.event.player.PlayerDropItemEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import de.maltesermailo.api.Context;
import de.maltesermailo.api.minigame.PlayerHandle;
import de.maltesermailo.api.minigame.PlayerScoreboard;
import de.maltesermailo.api.utils.ItemFactory;
import de.nightloewe.bedwars.Bedwars;
import de.nightloewe.bedwars.BedwarsMap;
import de.nightloewe.bedwars.BedwarsPlayerData;
import de.nightloewe.bedwars.BedwarsTeam;
import de.nightloewe.bedwars.gamephases.BedwarsLobbyGamePhase;
import net.md_5.bungee.api.ChatColor;

public class TeamChooserItem implements Listener {

	private Context ctx;
	
	private ItemStack teamChooserItem;
	
	public TeamChooserItem(Context ctx) {
		this.ctx = ctx;
		this.teamChooserItem = new ItemStack(Material.COMPASS);
		ItemMeta meta = this.teamChooserItem.getItemMeta();
		
		meta.setDisplayName(ChatColor.translateAlternateColorCodes('&', "&7Wähle dein Team"));
		this.teamChooserItem.setItemMeta(meta);
		
		Bukkit.getPluginManager().registerEvents(this, this.ctx.getPlugin());
	}
	
	public ItemStack getItem() {
		return teamChooserItem;
	}
	
	@EventHandler
	public void onInteract(PlayerInteractEvent e) {
		if(!this.ctx.miniGameController().getGamePhaseManager().isRunning() || !(this.ctx.miniGameController().getGamePhaseManager().current() instanceof BedwarsLobbyGamePhase)) {
			return;
		}
		if(e.getItem() != null 
				&& e.getItem().getType() == Material.COMPASS
				&& e.getItem().hasItemMeta()
				&& e.getItem().getItemMeta().getDisplayName().equalsIgnoreCase(this.teamChooserItem.getItemMeta().getDisplayName())) {
			//Getting amount of teams by arraylist size
			int teamsAmount = ((BedwarsMap) this.ctx.mapFactory().current()).getTeams().size();
			
			//Inventory
			Inventory inv = null;
			
			//Creating inventory by size of teams
			if(teamsAmount < 9) {
				inv = Bukkit.createInventory(e.getPlayer(), 9, "Wähle dein Team");
			} else {
				inv = Bukkit.createInventory(e.getPlayer(), 36, "Wähle dein Team");
			}
			
			//Cancelling if error occured
			if(inv == null) {
				e.getPlayer().sendMessage("&cError occured while opening team chooser, please contact an developer or administrator.");
				e.setCancelled(true);
				return;
			}
			
			//Adding teams to inventory
			for(int i = 0; i < teamsAmount; i++) {
				BedwarsTeam team = ((BedwarsMap) this.ctx.mapFactory().current()).getTeams().get(i);
				char teamColor = team.getDisplayName().charAt(1);
				DyeColor woolColor = TeamChooserItem.asWoolColor(teamColor);
				
				ItemStack teamItem = ItemFactory.newFactory(Material.WOOL, team.getDisplayName()).woolColor(woolColor).build();
				inv.setItem(i, teamItem);
			}
			e.getPlayer().openInventory(inv);
			e.setCancelled(true);
		}
	}
	
	@EventHandler
	public void onInventoryClick(InventoryClickEvent e) {;
		if(e.getInventory().getTitle() != null 
				&& e.getInventory().getTitle().equalsIgnoreCase("Wähle dein Team")
				&& e.getCurrentItem() != null
				&& e.getCurrentItem().getType() == Material.WOOL) {
			e.setCancelled(true);
			String teamName = e.getCurrentItem().getItemMeta().getDisplayName();
			
			for(BedwarsTeam team : ((BedwarsMap) this.ctx.mapFactory().current()).getTeams()) {
				if(teamName.equalsIgnoreCase(ChatColor.translateAlternateColorCodes('&', team.getDisplayName()))) {
					if(team.getTeamSize() == team.getMaxPlayers()) {
						e.getWhoClicked().closeInventory();
						Bedwars.instance().sendMessage((Player) e.getWhoClicked(), "&cDieses Team ist bereits voll");
						return;
					}
					
					Player p = (Player) e.getWhoClicked();
					PlayerHandle handle = this.ctx.miniGameController().getPlayerHandle(p);
					BedwarsPlayerData data = handle.getPlayerData();
					
					if(data.getTeam() != null 
							&& data.getTeam() == team) {
						Bedwars.instance().sendMessage((Player) e.getWhoClicked(), "&cDu bist bereits in diesem Team.");
						e.getWhoClicked().closeInventory();
						return;
					}
					
					data.setTeam(team);
					
					PlayerScoreboard sb = handle.getCurrentScoreboard();
					sb.removeScore(6);
					sb.setScore(6, ChatColor.translateAlternateColorCodes('&', team.getDisplayName()), -1);
					
					Bedwars.instance().sendMessage((Player) e.getWhoClicked(), "&aDu bist nun im Team " + teamName + "&a.");
					
					handle.refreshNick();
 				}
			}
		}
	}
	
	public void onInventoryDrag(InventoryDragEvent e) {
		if(!this.ctx.miniGameController().getGamePhaseManager().isRunning() || !(this.ctx.miniGameController().getGamePhaseManager().current() instanceof BedwarsLobbyGamePhase)) {
			return;
		}
		e.setResult(Result.DENY);
		e.setCancelled(true);
	}
	
	public void onInventoryDrop(PlayerDropItemEvent e) {
		if(!this.ctx.miniGameController().getGamePhaseManager().isRunning() || !(this.ctx.miniGameController().getGamePhaseManager().current() instanceof BedwarsLobbyGamePhase)) {
			return;
		}
		e.setCancelled(true);
	}
	
	/**
     * Converts color code into wool color
     *
     * @param color code
     * @return the wool color
     */
    public static DyeColor asWoolColor(char id) {
        switch (id) {
            case '0':
                return DyeColor.BLACK;
            case '1':
            	return DyeColor.BLUE;
            case '2':
                return DyeColor.GREEN;
            case '3':
                return DyeColor.BLUE;
            case '4':
                return DyeColor.RED;
            case '5':
                return DyeColor.PURPLE;
            case '6':
                return DyeColor.ORANGE;
            case '7':
                return DyeColor.SILVER;
            case '8':
            	return DyeColor.GRAY;
            case '9':
            	return DyeColor.BLUE;
            case 'a':
            	return DyeColor.GREEN;
            case 'b':
            	return DyeColor.CYAN;
            case 'c':
            	return DyeColor.RED;
            case 'd':
            	return DyeColor.PINK;
            case 'e':
            	return DyeColor.YELLOW;
            default:
                return DyeColor.WHITE;
        }
    }
}
